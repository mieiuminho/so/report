# Servidor de Vendas {#sec:sv}

O servidor de vendas é responsável por fazer a gestão de stocks dos artigos
existentes. Garante que são processados os pedidos de cada cliente comunicados
através da estrutura `LCliente`, se o pedido de um dado cliente for para
atualizar o stock de um artigo então é escrita uma linha de venda no ficheiro de
vendas.

Também é responsável por garantir a capacidade de vários clientes realizarem
pedidos em simultâneo e receberem as respostas a esses mesmos pedidos sem que se
perca informação.

O servidor recebe dois sinais diferentes. Para receber estes sinais o servidor
escreve num ficheiro o seu PID para que a manutenção possa comunicar com ele.
Podem ser enviados sinais para agregar o ficheiro de vendas desde a ultima
agregação e guardar o resultado num ficheiro com o nome correspondente ao
momento em que a agregação foi pedida, ou então para comunicar ao servidor que o
preço de um artigo mudou para que este possa mudar o preço do mesmo caso ele
esteja na cache.

A terminação do processo do servidor é efetuada se receber um sinal `SIGTERM`.
Nesse momento, antes de sair, verifica se algum cliente está a meio de um pedido
e se sim deixa-o terminar; Em seguida, fecha o canal de comunicação com os
clientes e responde aos pedidos que já estavam feitos e fila de espera. A
sincronização dos clientes é feita através da espera passiva por resposta do
servidor.

O servidor mantém uma *cache* de preços dos 100 artigos usados mais
recentemente. Desta forma, operações que utilizem o preço de um desses artigos
evita a leitura desse preço do ficheiro `artigos`.

Quando são efetuados pedidos de agregação, o servidor cria um processo de
agregação (um por cada 500 linhas a agregar) e distribuí por estes essas linhas.
No fim, faz a junção dessas agregações.

O ficheiro `artigos` mantém a posição do nome dos artigos que estão guardados no
ficheiro `strings`. Sempre que um nome de um artigo é atualizado, é escrito no
final do ficheiro `strings` e a posição é atualizada no ficheiro `artigos`.
Assim, o antigo nome torna-se obsoleto. Quando o espaço ocupado pelos nomes
obsoletos excede 20% do total do espaço ocupado pelo ficheiro é efetuada a
compactação. Esta escreve num novo ficheiro só os nomes válidos dos artigos e
atualiza as posições no ficheiro `artigos`.

