\appendix

# Testes de Desempenho

Os valores dos testes apresentados resultam executar o mesmo teste 20 vezes no
mesmo computador.

## Manutenção de Artigos

| Quantidade | Média  | $\sigma$ | Min    | Max    |
|-----------:|-------:|---------:|-------:|-------:|
|      1 000 |  0.015 |    0.002 |  0.013 |  0.018 |
|     10 000 |  0.143 |    0.004 |  0.136 |  0.152 |
|    100 000 |  1.419 |    0.013 |  1.401 |  1.453 |
|  1 000 000 | 14.410 |    0.080 | 14.254 | 14.562 |

: Tempo (em segundos) de inserir artigos pelo programa de manutenção
de artigos.

| Quantidade | Média  | $\sigma$ | Min    | Max    |
|-----------:|-------:|---------:|-------:|-------:|
|      1 000 |  0.028 |    0.003 |  0.024 |  0.036 |
|     10 000 |  0.266 |    0.008 |  0.256 |  0.287 |
|    100 000 |  2.660 |    0.016 |  2.635 |  2.686 |
|  1 000 000 | 15.805 |    0.120 | 15.594 | 16.043 |

: Tempo (em segundos) de trocar preços de artigos pelo programa de manutenção
de artigos.

| Quantidade | Média  | $\sigma$ | Min    | Max    |
|-----------:|-------:|---------:|-------:|-------:|
|      1 000 |  0.033 |    0.002 |  0.030 |  0.037 |
|     10 000 |  0.316 |    0.004 |  0.308 |  0.325 |
|    100 000 |  3.142 |    0.014 |  3.121 |  3.167 |
|  1 000 000 | 31.985 |    0.151 | 31.729 | 32.341 |

: Tempo (em segundos) de trocar nomes de artigos pelo programa de manutenção
de artigos.

\newpage


## Agregador de Vendas

| Quantidade | Média  | $\sigma$ | Min    | Max    |
|-----------:|-------:|---------:|-------:|-------:|
|      1 000 |  0.004 |    0.001 |  0.003 |  0.008 |
|     10 000 |  0.033 |    0.002 |  0.030 |  0.038 |
|    100 000 |  0.272 |    0.004 |  0.268 |  0.284 |
|  1 000 000 |  2.287 |    0.024 |  2.257 |  2.372 |
| 10 000 000 | 22.330 |    0.191 | 22.092 | 22.899 |

: Tempo (em segundos) de agregar linhas de vendas pelo agregador de forma
sequencial.

| Quantidade | Média  | $\sigma$ | Min    | Max    |
|-----------:|-------:|---------:|-------:|-------:|
|      1 000 |  0.007 |    0.001 |  0.003 |  0.009 |
|     10 000 |  0.037 |    0.011 |  0.029 |  0.041 |
|    100 000 |  0.576 |    0.121 |  0.385 |  0.759 |
|  1 000 000 |  4.243 |    1.100 |  2.157 |  5.862 |
| 10 000 000 | 21.211 |    5.122 | 11.055 | 38.244 |

: Tempo (em segundos) de agregar linhas de vendas pelo agregador de forma
concorrente.
