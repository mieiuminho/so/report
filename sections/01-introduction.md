# Introdução {#sec:intro}

O projeto prático de *Sistemas Operativos* consiste no desenvolvimento de um
sistema de gestão de inventário e vendas. Este sistema é formado por quatro
programas: manutenção de artigos, servidor de vendas, cliente de vendas e
agregador de dados.

A organização deste projeto segue uma arquitetura *cliente-servidor* para
tornar possível a gestão das vendas de artigos. Existe um programa responsável
pela manutenção de artigos que permite adicionar e alterar o preço e o nome dos
mesmos. O cliente de vendas fornece a possibilidade de fazer pedidos de consulta
e alteração de *stock* ao servidor. Assim sendo, o servidor desempenha um papel
fundamental nesta gestão de inventário e vendas.  Por último, existe um programa
capaz de fazer a agregação de vendas do mesmo artigo que é chamado pelo servidor
quando o utilizador desejar.

Os programas foram desenvolvidos na linguagem de programação C, fazendo uso de
chamadas ao sistema operativo recorrendo a funções disponibilidades pelas suas
bibliotecas.

O relatório está organizado do seguinte modo. Na próxima secção será apresentado
o programa de manutenção de artigos. Na @sec:sv será explicado a implementação
do servidor de vendas. Na @sec:cv é descrito as funcionalidades e estruturação
do cliente de vendas. O agregador é abordado na @sec:ag. Por fim, comentários
conclusivos são apresentados na @sec:end.

