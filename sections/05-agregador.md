# Agregador {#sec:ag}

O programa de agregação é capaz de fazer agregação de linhas de venda que são
recebidas através do `STDIN`. Os seus resultados agregados são enviados para o
`STDOUT`.

Cada instância do agregador cria um ficheiro temporário (distinto através do seu
PID). Nesse ficheiro é mantido o resultado da agregação, linha a linha, das
vendas recebidas até ao momento.

A função `agrega` é responsável por receber os componentes de uma linha de venda
e, ir ao ficheiro temporário verificar se este código já foi usado e se sim, é
lido o que tinha sido agregado e feita a soma da quantidade e do montante. Se
não tiver ainda sido lido, é escrita a linha de venda na posição do ficheiro
indexada pelo seu código e, guardado esse código num *array dinâmico* de códigos
usados.

```C
int agrega(unsigned long int codigo, long int quantidade, float montante);
```

Para o agregador receber `EOF` significa que não existem mais linhas de venda
para agregar, enviando o resultado pelo `STDOUT`.

```C
void show_vendas_agregadas();
```

Esta função percorre o *array dinâmico* com os códigos usados, indo à posição do
ficheiro temporário que corresponde ao código, fazendo a leitura da linha de
venda e enviando para o `STDOUT` as linhas de vendas agregadas.

