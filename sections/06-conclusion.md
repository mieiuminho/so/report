# Conclusão {#sec:end}

Os objetivos deste projeto foram alcançados, uma vez que, através da aplicação
prática dos conhecimentos adquiridos na Unidade Curricular, foram criadas
respostas de implementação para os desafios apresentados inicialmente pela
equipa docente.

A introdução de um sistema de *cache* de preços não levou a uma diminuição dos
tempos do servidor na sua resposta a pedidos. O facto de cada pedido ser
realizado em muito pouco tempo faz com que não existam grandes margens para
ganhos. Além disso, os computadores usados para as medições possuem *Solid-State
Drive* (SSD) com tempos de acesso bastante reduzidos. Os SSD são, por isso, uma
das razões para não existirem ganhos, visto que, a *cache* estaria a reduzir o
número de leituras de disco.

A programação concorrente exigiu que fossem tomadas restrições na partilha dos
mesmos recursos de dados. Analisando os resultados obtidos no apêndice A, é
possível concluir que a agregação concorrente não traz benefícios evidentes
devido à sobrecarga do sistema na gestão dos processos criados. Os testes
realizados incluíam casos de input com diversas linhas de venda repetidas o que,
de facto, deveria beneficiar a agregação concorrente.

Este projeto levou ao um entendimento mais profundo dos componentes que formam
os sistemas operativos, tais como, processos concorrentes, instruções
assíncronas, o sistema de ficheiros e memória volátil e persistente.

