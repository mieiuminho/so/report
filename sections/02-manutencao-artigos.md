# Manutenção de Artigos {#sec:ma}

O programa de manutenção de artigos implementa funcionalidades de gestão. As
suas funcionalidades englobam a capacidade de inserir novos, alterar o nome e os
preços dos artigos previamente inseridos. Além disso, é capaz de pedir que seja
feita a agregação de vendas.

```
$ ./bin/ma
i <nome> <preço>        --> insere novo artigo e mostra código
n <código> <novo nome>  --> altera nome do artigo
p <código> <novo preço> --> altera preço do artigo
a                       --> pede ao servidor para fazer agregação de vendas
...
<EOF>
```

## Ficheiro Strings

O ficheiro de strings contém no início uma variável com o espaço obsoleto até ao
momento, ou seja, o espaço ocupado pelos nomes de artigos que foram alterados
entretanto.

```C
unsigned long int obsoleteSpace = 0;
```

Após este valor, estão escritos os nomes dos artigos. Cada entrada neste
ficheiro, contém o nome do artigo precedido do comprimento desse nome.

## Ficheiro Artigos

O ficheiro artigos contém uma variável com o código do próximo artigo a inserir
e, uma variável com a a posição até onde já foi escrito no ficheiro de strings.
Sendo ambas variáveis do tipo `unsigned long int`.

```C
unsigned long int posicaoFicheiro = 0;
unsigned long int proximoCodigo = 1;
```

Seguindo esses valores contém artigos contíguos. Os artigos seguem a seguinte
estrutura:

```C
typedef struct artigo {
    int posicaoString;
    float preco;
} Artigo;
```

## Ficheiro Stocks

O ficheiro de stocks contém as quantidades dos artigos existentes pela ordem de
inserção desses mesmos artigos.

## Implementação

A capacidade de inserir artigos é implementada pela função seguinte. Esta função
é responsável pela criação dos ficheiros `artigos`, `strings` e `stocks`, no
caso de ainda não existirem.

```C
unsigned long int insereArtigo(char *nome, char *preco);
```

No caso do ficheiro de artigos já existir, é lido o código do próximo artigo a
inserir e o valor da posição do ficheiro de strings onde inserir. Caso
contrário, a posição do ficheiro de strings passa a ser 0 e o próximo código é
1.

A inserção de um artigo passa por atualizar o valor do próximo código e a
posição do ficheiro de `strings` onde escrever. Após isso, guarda a informação
desse novo artigo nos ficheiros necessários.

A possibilidade de alterar o preço de um artigo identificado pelo seu código
passa por verificar se o ficheiro de artigos já está criado, e se sim, verificar
que esse artigo existe. Se respeitar ambas as condições, esse artigo é lido e
modificado o campo do preço e escrito de seguida na mesma posição do ficheiro de
artigos.

```C
int alteraPreco(unsigned long int codigo, float preco);
```

A função `alteraNome` permite a modificar o nome de um artigo identificado
através do seu código. É verificado a existência do ficheiro de artigos e
strings e a existência de um artigo identificado pelo código pedido. Se todas as
condições forem respeitadas é escrito na próxima posição do ficheiro de strings
o novo nome (precedido do comprimento)

```C
int alteraNome(unsigned long int codigo, char *nome);
```

Através do manutenção de artigos é possível pedir ao servidor para fazer a
agregação das linhas de venda.

```C
int talkToServer(int sinal);
```

O pedido é feito através do envio de um sinal `SIGUSR1` ao servidor. O PID do
servidor é obtido através da leitura de um ficheiro escrito pelo servidor. O
servidor está preparado para receber esse sinal fazendo a agregação.

