# Cliente de Vendas {#sec:cv}

Recebe pedidos através do `STDIN` que comunica ao servidor e mostra os
resultados obtidos pelo `STDOUT`. Os pedidos contém sempre o código do artigo
associado. Se o pedido apenas contiver o código, é mostrado o seu stock e preço.
Caso o código venha acompanhado de uma quantidade é pedido ao servidor para
atualizar o stock desse artigo; O servidor envia o novo stock ao cliente de
vendas e este mostra-o.

```
$ ./bin/cv
<código_numérico>              --> mostra no stdout stock e preço
<código_numérico> <quantidade> --> actualiza stock e mostra novo stock
...
<EOF>
```

Os pedidos são comunicados ao servidor através da escrita num pipe com nome
criado pelo servidor. Essa escrita contém a estrutura `LCliente`.

```C
typedef struct linhaCliente {
   char operacao;
   unsigned long int codigo;
   long int quantidade;
   char nomeCliente[128];
} LCliente;
```

O primeiro campo desta estrutura contém um `char` identificativo. Se este campo
contiver um *a*, é porque se trata de uma atualização de *stock*, se contiver um
*c* é porque se trata de uma consulta de *stock*.

O segundo campo contém o código do artigo e o terceiro a quantidade (caso seja
necessário para a operação).

O último campo, contém o nome do pipe criado pelo cliente que o servidor deverá
usar para enviar as respostas.

As respostas são enviadas na forma das estruturas de `LinhaAtualiza` e
`LinhaConsulta`.

```C
typedef struct linhaAtualiza {
    unsigned long int codigo;
    unsigned long int stock;
} LinhaAtualiza;
```

```C
typedef struct linhaConsulta {
    unsigned long int stock;
    float preco;
} LinhaConsulta;
```

